# Exploration de l'OS et du matériel

**Ce TP est un TP à la carte.** C'est à dire que vous choisir de ne faire qu'un seul sujet. Libre à vous d'en faire plusieurs.

**Il est fortement recommander de lire la description de chacun des sujets avant d'en choisir un.**

Aussi, des recherches en autonomie seront nécessaires pour mener chacun des sujets à bien. **N'hésitez pas à faire appel à un ami (moi) mais demandez à Google avant.**

Dernier détail, **POUR RAPPEL**, bien que certains éléments du TP fassent référence à des OS GNU/Linux de façon spécifique (plutôt que Windows ou autres), c'est dans un but **pédagogique**. Les systèmes GNU/Linux exposent naturellement beaucoup plus de choses à l'utilisateur, et lui laisse un pouvoir total sur la machine. On a besoin de ça pour ce TP.  
**Les autres OS utilisent strictement les mêmes concepts.**

# Sommaire

<!-- vim-markdown-toc GitLab -->

* [Sujet 1 : Ecrire de l'assembleur](#sujet-1-ecrire-de-lassembleur)
    * [Présentation du sujet](#présentation-du-sujet)
    * [Préliminaires](#préliminaires)
    * [Exercices](#exercices)
        * [Exo 1](#exo-1)
        * [Exo 2](#exo-2)
        * [Exo 3](#exo-3)
        * [Exo 4](#exo-4)
* [Sujet 2 : Débugger et désassembler des programmes compilés](#sujet-2-débugger-et-désassembler-des-programmes-compilés)
    * [Présentation du sujet](#présentation-du-sujet-1)
    * [Préliminaire](#préliminaire)
    * [Exercices](#exercices-1)
        * [Hello World](#hello-world)
        * [Winrar crack](#winrar-crack)
* [Sujet 3 : Récupérer et analyser la mémoire vive](#sujet-3-récupérer-et-analyser-la-mémoire-vive)
    * [Présentation du sujet](#présentation-du-sujet-2)
    * [Préliminaire](#préliminaire-1)
    * [Exercices](#exercices-2)
* [Sujet 4 : Analyse de boot](#sujet-4-analyse-de-boot)
    * [Présentation du sujet](#présentation-du-sujet-3)
    * [Préliminaire](#préliminaire-2)
    * [Exercices](#exercices-3)
        * [Exo 1](#exo-1-1)
        * [Exo 2](#exo-2-1)
* [Sujet 5 : Manipulations autour des appels système](#sujet-5-manipulations-autour-des-appels-système)
    * [Présentation du sujet](#présentation-du-sujet-4)
    * [Préliminaire](#préliminaire-3)
    * [Intro aux syscalls](#intro-aux-syscalls)
    * [Exercices](#exercices-4)
        * [Exo 1 : Lecture d'un fichier](#exo-1-lecture-dun-fichier)
        * [Exo 2 : Modification d'un fichier](#exo-2-modification-dun-fichier)
        * [Exo 3 : Manipulation de droits](#exo-3-manipulation-de-droits)
        * [Exo 4 : `ping`](#exo-4-ping)

<!-- vim-markdown-toc -->

# Sujet 1 : Ecrire de l'assembleur

## Présentation du sujet

Ici il est question d'apprendre les rudiments du langage machine, ou plutôt du langage assembleur.

Pour ce faire, quelques notions théoriques d'abord avant d'appréhender le fonctionnement et les principales instructions.

Puis la réalisation de quelques programmes en assembleur, en utilisant une application afin d'émuler un processeur.

> On émule un processeur afin d'appréhender l'assembleur de façon simple (simple d'utilisation, et simple à comprendre) sans ajouter les particularités de chacun de vos processeurs. Pour le TP, c'est plus simple !

## Préliminaires

* logique booléenne
  * strictement essentiel d'avoir en tête le principe de la logique booléenne
    * valeurs "vrai" ou "faux"
    * opérations logiques "et", "ou" et "non"
* instructions assembleur basique
  * arithmétique
    * ADD, MUL, etc
  * logique 
    * AND, OR, NOT

Vous aurez besoin de télécharger le logiciel Emu8086 pour émuler un processeur (architeture 8086) et coder votre propre assembleur.

## Exercices

### Exo 1

En utilisant uniquement des OR, AND, NOT, créer un code qui permet de réaliser un XOR.

### Exo 2

En utilisant uniquement le opérations logiques OR, AND, NOT et XOR, créer un additionneur.

### Exo 3

Créer un programme qui affiche dans la sortie standard une chaîne de caractère définie dans le code.

### Exo 4 

Créer une boucle qui calcule et affiche les chiffres de 1 à 10.

# Sujet 2 : Débugger et désassembler des programmes compilés

## Présentation du sujet

Quelques définitions s'imposent :
* **débugger** : exécuter un programme "pas à pas", c'est à dire instruction par instruction
  * cela permet de voir l'état des variables internes au programme en temps réel
  * cela peut aussi permettre de modifier le comportement du programme à la volée, pendant son exécution
* **désassembler** : regarder le code machine (l'assembleur) d'un programme compilé
  * on peut alors étudier la logique interne du programme

Ce sujet sera donc dédié à l'assembleur, mais plutôt sur l'étude de codes déjà écrits, *via* l'utilisation d'outils permettant de débugger ou désassembler des programmes. Ce sont un peu les rudiments d'une discipline de la sécurité offensive appelée *cracking*.

## Préliminaire

* logique booléenne
  * strictement essentiel d'avoir en tête le principe de la logique booléenne
    * valeurs "vrai" ou "faux"
    * opérations logiques "et", "ou" et "non"
* instructions assembleur basique
  * arithmétique
    * ADD, MUL, etc
  * logique 
    * AND, OR, NOT

Vous aurez besoin d'un débuggeur et un désassembleur pour ce sujet. Il en existe des tas, je vous conseille : 
* débugger : gdb (avec gdb-peda éventuellement)
* désassembleur : ghidra ou IDA

## Exercices

### Hello World

* commencer par coder puis compiler un programme Hello World en C
  * ce programme doit simplement afficher "Hello World!" dans le terminal lorsqu'il est lancé
* désassembler le programme et mettre en évidence 
  * où est stockée la chaîne de caractère "Hello World"
  * comment elle est affichée dans le terminal

### Winrar crack

Winrar est un programme qui permet de manipuler des archives (`.rar`, `.zip`, etc).

Winrar est un programme payant qui possède une version d'évaluation. Une fois cette période d'évaluation dépassée, le programme nous rappelle régulièrement qu'il est nécessaire d'acheter le produit pour continuer à l'acheter (oupa).

Le but de cette section est de crack Winrar afin d'avoir une version utilisable comme une version achetée. (j'ai testé avec la version que j'avais sous la main : 5.4)

Il existe 10000 tutos pour crack Winrar sur le web, je vous laisse partir sur ça. C'est un peu le cas d'école de référence :)

# Sujet 3 : Récupérer et analyser la mémoire vive

## Présentation du sujet

La mémoire vive contient toutes les données "chaudes" d'un système, c'est-à-dire les données en cours d'utilisation (un document texte ouvert, une application en train de s'exécuter, une vidéo en train d'être lue, etc.). En récupérant la mémoire vive d'une machine, on peut récupérer beaucoup d'informations sur le système.

Récupérer la mémoire vive d'un système se fait *via* un "dump" de RAM. 

Dans la vie réelle, cela peut être utile dans plusieurs cas :
* étude post-mortem d'un système
  * beaucoup de systèmes sont configurés pour générer un dump de la RAM lors d'un crash
  * on peut alors aller étudier les raisons du crashes
* analyses forensiques
  * analyses visant à récupérer des informations sur le système hôte
  * sécurité offensive et défensive
  * analyse de malware
* permettre, dans un cadre d'apprentissage, de voir comment est réellement structurée la mémoire vive
  * et ainsi permettre la mise en place de nouvelles mesures de sécurité ou des optimisations

Dans cette partie il sera question de réaliser un dump de RAM d'une machine Windows 10 (ou Ubuntu si certains préfèrent), puis de l'analyser en partie à la main, et en partie avec des outils spécialisés.

## Préliminaire

Afin d'éviter d'avoir trop de résultats différents, vous récupérerez un dump de RAM d'un OS Windows 10 (en version famille de préférence) ou une version LTS récente de Ubuntu.

Pour effectuer le dump, je vous laisse libre sur la méthode. Cela dit :
* il est souvent plus simple de réaliser un dump de la mémoire d'une VM plutôt qu'une machine physique
* vous pouvez le faire avec votre hôte, si c'est le cas, c'est plus simple sous GNU/Linux que sous Windows

---

Je vous conseille de 
* réaliser le dump avec VirtualBox
  * mettez pas trop de RAM (512M, 1G, par là) à la VM, ça permettra d'accélérer l'analyse du dump plus tard (il sera moins gros)
* utiliser un OS Windows 10 installé fraîchement dans une VM (ou Ubuntu, en version LTS).
* **aussi, pour avoir un peu plus de matière, lancez des programmes avant de réaliser le dump.** Genre un bel internet explorer (Edge !) avec deux trois onglets ouverts :)
* analyser le dump avec [Volatility](https://github.com/volatilityfoundation/volatility) (ligne de commande).

---

Pour Volatility, on commence généralement par déterminer le "profil" du dump de RAM (=> c'est une référence précise à l'OS depuis lequel le dump a été extrait) :

```bash
$ volatility -f <YOUR_DUMP> imageinfo
```

Pour la suite, vous trouverez des bonnes docs sur la toile, la plus exhaustive étant bien entendu [la doc officielle sur le wiki](https://github.com/volatilityfoundation/volatility/wiki/Command-Reference).

## Exercices

* déterminer la liste des processus lancés
* déterminer la liste des fichiers ouverts sur la machine
  * extraire un fichier et essayer de récupérer son contenu
* récupérer des clés registre (Windows only)
* trouver au moins une autre info intéressante à extraire du dump

# Sujet 4 : Analyse de boot

## Présentation du sujet

Dans ce sujet, vous allez vous intéresser plus en détail à la chaîne de boot que l'on a appréhendé lors du cours théorique.

Au menu : 
* exploration par vous-même des systèmes de boot Windows et GNU/Linux
* exploration des logs noyau du démarrage avec une machine GNU/Linux

## Préliminaire

Avoir une machine GNU/Linux sous la main. De préférence une machine physique pour avoir une chaîne de boot vraiment intéressante.

Sinon une VM ira très bien, avec une interface graphique pour avoir un boot plus fourni. Vous pouvez réutiliser les CentOS7 du cours, mais un petit Ubuntu avec un interface graphique c'est mieux :)

Pour analyser la chaîne de boot, on va utiliser deux choses :
* les logs du boot généré par le noyau
  * on peu tles consulter avec la commande `dmesg`
* systemd (système "init" dans beaucoup de GNU/Linux récents, gestionnaire de processus/daemons etc.)
  * on peut le manipuler avec des commandes comme `systemctl`

## Exercices

### Exo 1

**Déterminer les 5 processus les plus longs à démarrer au boot de l'OS.** 

Vous pouvez utiliser la commande :

```bash
$ systemd-analyse plot > graphe.svg
```

Cette commande va générer un fichier `graphe.svg` qui contient un graphe représentant les différents processus qui se sont lancés au démarrage de l'OS. Vous pouvez l'ouvrir avec un navigateur web.

### Exo 2

**Analyser les logs de boot du noyau** (avec la commande `dmesg`) :
* déterminer la liste des périphériques vus par le noyau, et l'ordre dans lequel ils sont remontés
* repérer la version du noyau utilisé
* choisir et expliquer 5 autres lignes qui vous semblent importantes
* est-ce que le kernel continue à générer des logs une fois la machine allumée ?

# Sujet 5 : Manipulations autour des appels système

## Présentation du sujet

Ici vous allez explorer les appels système réalisés par un OS GNU/Linux afin de comprendre un peu mieux :
* les interactions entre l'OS et le noyau
* le rôle du noyau
* comment votre espace utilisateur fonctionne

## Préliminaire

Une VM GNU/Linux, n'importe laquelle.

Quelques recherches sur l'utilité et le fonctionnement des syscalls.

## Intro aux syscalls

Un *syscall* (ou appel système en fraçais) est une fonction exposée par le kernel, dans le but d'être utilisée par l'OS ou ses applications. Par extension, c'est donc tout ce qu'il est capable de demander au kernel. 

L'OS étant manipulé par les applications et l'utilisateur, les syscalls représentent notamment toutes les opérations qu'un utilisateur peut effectuer sur un système. 

La liste des syscalls est plutôt longue, en voici quelques exemples : 
* `open()` ouvrir un fichier
* `read()` lire un fichier
* `fork()` créer un nouveau processus
* `mmap()` charger en RAM un fichier (ou une partie de fichier)
* `setuid()` interagir avec le *setuid bit* des fichiers
* `chown()` changer le propriétaire d'un fichier

## Exercices

Pour les exercices vous pouvez utiliser la commande `strace` afin de tracer les appels système effectués par une application donnée. Utilisation : 

```bash
# Lire un fichier
$ cat /tmp/testfile

# Lire un fichier et tracer les appels système que le programme 'cat' effectue
$ strace cat /tmp/testfile

# Lire un fichier, tracer les syscalls, et les enregistrer dans un fichier plutôt qu'un affichage dans le terminal
$ strace -o /tmp/output_file cat /tmp/testfile
```

---

La plupart des exercices consistent à taper une commande, enregistrer ("tracer") tous les appels système qu'elle effectue, puis analyser le résultat.

J'utilise la formulation ***"mettre en évidence les principaux syscalls"***. Cela veut dire :
* les syscalls vraiment essentiels au bon fonctionnement de la commande
  * il y en a peu pour chacune des commandes
  * essayez d'en comprendre le plus possible, pour extrare l'essentiel de façon pertinente
* par exemple, pour une lecture de fichier on va chercher par exemple :
  * le syscall qui lit le fichier
  * le syscall qui permet d'afficher le contenu dans le terminal
  * le syscall qui effectue un test de permission 

J'attends de vous de :
* pour chaque syscall
  * donner le nom du syscall
  * donner sa fonction, son utilité
  * expliquer tous ses arguments
* vous pouvez m'afficher le `strace` complet, puis un `strace` allégé où ne figurent que les syscalls principaux

### Exo 1 : Lecture d'un fichier

Mettre en évidence les principaux syscalls lors d'une commande `cat <FICHIER>`.

### Exo 2 : Modification d'un fichier

Mettre en évidence les principaux syscalls lors de :

```bash
# Cas 1 : écriture depuis la ligne de commande
echo "test" > /tmp/testfile

# Cas 2 : écriture avec un éditeur de texte
vim /tmp/testfile
```

**Hint** : pour le cas 2, vous pouvez :

```bash
# Dans un premier terminal
# Il servira à utiliser vim, et à générer le fichier contenant tous les syscalls
$ strace -o /tmp/output_file vim /tmp/testfile

# Dans un deuxième terminal, il servira à visualiser en temps réel les syscalls enregistrés
$ tail -f /tmp/output_file
```

### Exo 3 : Manipulation de droits

Pour chacune des commandes suivantes :
* expliquer à quoi elle sert
* mettre en évidence les principaux syscalls

```bash
$ chmod 700 /tmp/testfile

$ chmod 4700 /tmp/testfile

$ chown root /tmp/testfile

$ chown root:root /tmp/testfile
```

### Exo 4 : `ping`

Mettre en évidence les principaux syscalls lors de : 

```bash
$ ping 8.8.8.8

$ ping www.cnrtl.fr # c'est juste un nom de domaine random que je suis à peu près sûr que vous avez jamais résolu avec votre VM
```
