# Maîtrise de poste - Day 1

L'objectif de ce TP est de mieux vous faire appréhender l'OS dans lequel vous évoluer tous les jours.

On va explorer :
* les informations liées à votre OS (liste de périphériques, utilisateurs, etc.)
* l'utilisation des périphériques (partitions de disque, port réseau, etc.)
* la sécurité (sujets autour du chiffrement, la signature, etc.)
* le téléchargement de logiciels (gestionnaire de paquets)
* le contrôle à distance (SSH)
* exposition de service réseau local
* la notion de certificats et de confiance (TLS, chiffrement de mails)

## Index 

> Je vous épargne un passage sur `git` parce que vous en avez déjà eu un ? Mais on verra...

<!-- vim-markdown-toc GitLab -->

* [Self-footprinting](#self-footprinting)
    * [Host OS](#host-os)
    * [Devices](#devices)
    * [Network](#network)
    * [Users](#users)
    * [Processus](#processus)
* [Scripting](#scripting)
* [Gestion de softs](#gestion-de-softs)
* [Partage de fichiers](#partage-de-fichiers)
* [Chiffrement et notion de confiance](#chiffrement-et-notion-de-confiance)
    * [Chiffrement de mails](#chiffrement-de-mails)
    * [TLS](#tls)
* [SSH](#ssh)
    * [Serveur](#serveur)
    * [Client](#client)
* [SSH avancé](#ssh-avancé)
    * [SSH tunnels](#ssh-tunnels)
    * [SSH jumps](#ssh-jumps)
* [Forwarding de ports at home](#forwarding-de-ports-at-home)

<!-- vim-markdown-toc -->

# Self-footprinting

Première étape : prendre connaissance du système. Etapes peut-être triviales mais néanmoins nécessaires.

**Détailler la marche à suivre pour chacune des étapes.**

## Host OS

🌞 Déterminer les principales informations de votre machine 
* nom de la machine
* OS et version
* architecture processeur (32-bit, 64-bit, ARM, etc)
* modèle du processeur
* quantité RAM et modèle de la RAM

## Devices

Travail sur les périphériques branchés à la machine = à la carte mère.

🌞 Trouver
* la marque et le modèle de votre processeur
  * identifier le nombre de processeurs, le nombre de coeur
  * si c'est un proc Intel, expliquer le nom du processeur (oui le nom veut dire quelque chose)
* la marque et le modèle :
  * de votre touchpad/trackpad
  * de vos enceintes intégrées
  * de votre disque dur principal

🌞 Disque dur
* identifier les différentes partitions de votre/vos disque(s) dur(s)
* déterminer le système de fichier de chaque partition
* expliquer la fonction de chaque partition

## Network

🌞 Afficher la liste des cartes réseau de votre machine
* expliquer la fonction de chacune d'entre elles

🌞 Lister tous les ports TCP et UDP en utilisation
* déterminer quel programme tourne derrière chacun des ports
* expliquer la fonction de chacun de ces programmes

## Users

🌞 Déterminer la liste des utilisateurs de la machine
* la liste **complète** des utilisateurs de la machine (je vous vois les Windowsiens...)
* déterminer le nom de l'utilisateur qui est full admin sur la machine
  * il existe toujours un utilisateur particulier qui a le droit de tout faire sur la machine

## Processus

🌞 Déterminer la liste des processus de la machine
* je vous épargne l'explication de chacune des lignes, bien que ça serait pas plus mal...
* choisissez 5 services système et expliquer leur utilité
  * par "service système" j'entends des processus élémentaires au bon fonctionnement de la machine
  * sans eux, l'OS tel qu'on l'utilise n'existe pas
* déterminer les processus lancés par l'utilisateur qui est full admin sur la machine

# Scripting

Le scripting est une approche du développement qui consiste à automatiser de petites tâches simples, mais réalisées à intervalles réguliers ou un grand nombre de fois. 

L'objectif de cette partie est de manipuler un langage de script natif à votre OS. Les principaux avantages d'utiliser un langage natif :
* bah... c'est natif ! Pas besoin d'installation
* le langage est mis à jour automatiquement, en même temps que le système
* le langage est rapide, car souvent bien intégré à l'environnement
* il permet d'accéder de façon simples aux ressources de l'OS (périphériques, processus, etc.) et de les manipuler de façon tout aussi aisée

Vous devrez *évidemment* :
* commenter votre script
* mettre un en-tête à votre script
  * shebang si besoin
  * auteur
  * date
  * description **succincte** de ce que fait le script
* éviter le plus possible que le script ait besoin de se lancer avec des privilèges élevés
  * on respecte le [principe du moindre privilège](https://en.wikipedia.org/wiki/Principle_of_least_privilege)
  * sous GNU/Linux vous n'avez normalement pas besoin de `root` pour ce script

🌞 Utiliser un langage de scripting natif à votre OS
* trouvez un langage natif par rapport à votre OS
  * s'il y en a plusieurs, expliquer le choix
* l'utiliser pour coder un script qui
  * affiche un résumé de l'OS
    * nom machine
    * IP principale
    * OS et version de l'OS
    * date et heure d'allumage
    * détermine si l'OS est à jour
    * Espace RAM utilisé / Espace RAM dispo
    * Espace disque utilisé / Espace disque dispo
  * liste les utilisateurs de la machine
  * calcule et affiche le temps de réponse moyen vers `8.8.8.8`
    * avec des `ping`

🐙 ajouter des fonctionnalités au script
* calcule et affiche le débit maximum en download et upload vers internet

Exemple d'output : 
```bash
$ ./footprinting.sh 
Get average ping latency to 8.8.8.8 server...
Get average upload speed...
Get average download speed...

----------------------------------------
nowhere.localhost footprinting report :
----------------------------------------
OS : Arch Linux
Linux kernel : 5.5.9-arch1-2
Is OS up-to-date : false
Up since : 2020-04-29 09:03:07

Interface wlp4s0 : 192.168.1.57/24
  - 8.8.8.8 average ping time : 15.623 ms
  - average download speed : 349.59 Mbit/s
  - average upload speed : 217.76 Mbit/s

RAM
  Used : 3.4Gi
  Free : 2.0Gi
Disk
  Used : 130G
  Free : 90G

Users list : root bin daemon mail ftp http nobody dbus systemd-journal-remote systemd-network systemd-resolve systemd-timesync systemd-coredump uuidd it4 avahi colord polkitd rtkit lightdm usbmux git cups nx nm-openvpn docker gluster rpc netdata dnsmasq toto redis test-cesi tor
```


---

🌞 Créer un deuxième script qui permet, en fonction d'arguments qui lui sont passés :
* exécuter une action
  * lock l'écran
  * éteindre le PC
* après X secondes

# Gestion de softs

Tous les OS modernes sont équipés ou peuvent être équipés d'un gestionnaire de paquets. Par exemple :
* `apt` pour les GNU/Linux issus de Debian
* `dnf` pour les GNU/Linux issus de RedHat
* `brew` pour macOS
* `chocolatey` pour Windows

🌞 Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets
* par rapport au téléchargement en direct sur internet
* penser à l'identité des gens impliqués dans un téléchargement (vous, l'éditeur logiciel, etc.)
* penser à la sécurité globale impliquée lors d'un téléchargement

🌞 Utiliser un gestionnaire de paquet propres à votre OS pour
* lister tous les paquets déjà installés
* déterminer la provenance des paquets (= quel serveur nous délivre les paquets lorsqu'on installe quelque chose)

# Partage de fichiers

Un serveur de fichiers permet de partager des fichiers sur le réseau. Il est bon de maîtriser un outil natif (ou très proche) de l'OS afin de lancer un partage ou y accéder. 

Les solutions les plus répandues :
* Windows : partage Samba
* GNU/Linux ou MacOS : NFS ou Samba

> Sur Windows ça peut se faire avec un simple 'Clic droit > Propriétés > Partager' ou quelque chose comme ça.

🌞 Monter et accéder à un partage de fichiers
* prouver que le service est actif
  * prouver qu'un processus/service est dédié à ce partage
  * prouver qu'un port réseau permet d'y accéder
* prouver qu'un client peut y accéder

# Chiffrement et notion de confiance

Le but de cette partie c'est de comprendre la valeur du cadenas vert lors des connexions HTTPS. Le but :
* comprendre la signification exacte et technique de ce cadenas
* pouvoir infirmer ou confirmer manuellement sa validité

---

Tous les systèmes modernes sont munis d'un magasin de **certificats**. C'est une liste de certificats auxquels on fait confiance d'emblée. Plus précisément :
* par "systèmes", on entend, entre autres :
  * la plupart des OS
  * la plupart des navigateurs web
* "faire confiance"
  * ça veut dire qu'on accepte de croire ce que nous dit cette personne
  * il existe souvent le concept de "les amis de mes amis sont mes amis"
    * donc on fait confiance à tous les gens à qui font confiance les membres de notre liste (le magasin de certificat)

🌞 Expliquer en détail l'utilisation de certificats
* quelle est l'information principale d'un certificat ?
  * le certificat transporte un élément essentiel, toutes les autres infos ne sont que des métadonnées
* quelles sont d'autres informations importantes pour la sécurité d'un certificat ?

## Chiffrement de mails

Le chiffrement de mail permet de garantir :
* la confidentialité d'un mail
* la provenance d'un mail
* l'intégrité d'un mail

La mise en place du chiffrement de mail va différer suivant le client mails que vous utilisez. Je ne vais donc pas détailler précisément la démarche ici, simplement donner les étapes essentielles.

🌞 En utilisant votre client mail préféré, mettez en place du chiffrement de mail
* vous aurez besoin d'une paire de clés
* mettre en place une signature numérique
  * réalisable avec une configuration de votre côté
* mettre en place du chiffrement de mail
  * réalisable lorsque le destinataire a effectué une configuration particulière

## TLS

On va utiliser l'utilisation du protocole TLS faite par HTTPS comme exemple de mise en oeuvre de ce procédé.

🌞 Expliquer
* que garantit HTTPS par rapport à HTTP ? (deux réponses attendues)
* qu'est-ce que signifie précisément et techniquement le cadenas vert que nous présente nos navigateurs lorsque l'on visite un site web "sécurisé"

🌞 Accéder à un serveur web sécurisé (client)
* accéder à un site web en HTTPS
* visualiser le certificat
  * détailler l'ensemble des informations et leur utilité
* valider **manuellement** le certificat 
  * visualiser les infos du certificat
  * déterminer ce qui permet de faire confiance au HTTPS de ce site web

🐙 Se renseigner sur le protocole TLS, et l'expliquer en une suite d'étapes simples.

# SSH

SSH n'est ici qu'un exemple pratique à mettre en oeuvre. Il est possible d'extrapoler les concepts abordés ici à d'autres services ou protocoles.

De plus, SSH est un protocole permettant de prendre la main sur un poste à distance, il est donc bon de savoir le manipuler afin d'avoir une maîtriser sur son poste en tant qu'outil d'administration.

## Serveur

**Monter un serveur SSH, une VM CentOS7 fera l'affaire.**

🐙 Sécuriser le serveur SSH (inspirez vous de guides sur internet [comme celui-ci de l'ANSI](https://www.ssi.gouv.fr/en/guide/openssh-secure-use-recommendations/))
* créer un fichier de configuration où **chaque ligne est comprise**
* livrer le fichier de configuration et expliquer chacun des lignes

## Client

* Générer une **nouvelle** paire de clés SSH
* Déposer la clé nécessaire sur le serveur pour pouvoir vous y connecter

🌞 Expliquer tout ce qui est nécessaire pour se connecter avec un échange de clés, **en ce qui concerne le client**
* quelle(s) clé(s) sont générée(s) ? Comment ?
* quelle clé est déposée ? Pourquoi pas l'autre ?
* à quoi ça sert **précisément** de déposer cette clé sur le serveur distant, **qu'est-ce qu'il va pouvoir faire, précisément** avec ?
* dans quel fichier est stocké la clé ? Quelles permissions sur ce fichier ?

🌞 Le fingerprint SSH
* c'est le "yes/no" lors de la première connexion à un serveur SSH
* expliquer le fonctionnement et l'utilité du fingerprint SSH

🌞 Créer un fichier `~/.ssh/config` et y définir une connexion
* permet de se connecter à la VM avec le nom `toto`
* utilise explicitement la paire de clé précédemments générée

# SSH avancé

Certaines fonctionnalités de SSH peuvent être très pratiques. Pendant votre cursus YNOV avec les VMs, mais pas que.  

Encore une fois les concepts vus ici peuvent être extrapolés pour comprendre d'autres outils.

## SSH tunnels

🌞 Mettez en place un serveur Web dans une VM
* connectez-vous à cette VM en SSH
* utilisez un port-forwarding local pour accéder au site web comme si vous y étiez
  * `ssh -D 7777 serveur`
  * configurez votre navigateur pour utiliser le proxy local sur le port `7777`
    * très facile à faire avec Firefox
  * visitez l'adresse `http://localhost` qui doit renvoyer sur le serveur web
* sur une CentOS7 avec internet, vous pouvez copier-coller la ligne plus bas, le but ici c'est pas de bosser sur un serveur web

```
$ sudo yum install -y epel-release && sudo yum install -y nginx && sudo systemctl enable --now nginx && sudo firewall-cmd --add-port=80/tcp --permanent && sudo firewall-cmd --reload
```

## SSH jumps

🌞 Mettez en place et testez un jump SSH
* expliquer le concept et l'utilité d'un jump SSH
* configurer un jump SSH dans votre fichier `~/.ssh/config`

# Forwarding de ports at home

Doit être réalisé en duo. Le but : bosser à deux sur la même VM, même si vous êtes pas ensemble.

🌞 Configurer une VM
* un serveur SSH
* une IP en bridge VitualBox (pas host-only, **bridge**)
  * expliquer le concept d'une IP bridge dans VirtualBox
* vérifier que la VM est joignable depuis le LAN correspondant

🌞 Configurer un port forwarding sur votre box
* allez sur l'interface d'administration de votre Box
* ouvrez un port firewall
* configurez un port forwarding pour qu'on puisse accéder à la VM en utilisant l'IP de votre box

🌞 Tester
* votre binôme se connecte à la machine virtuelle
* avec un tunnel SSH (`-D`), il peut effectuer des requêtes dans le LAN
* prouvez que : vous accédez à l'interface d'administration de la Box du lAN distant

> VPN SSH maison, c'est pas beau ça ? It works perfectly fine.

🐙 `tmux`
* because why not
* on peut utiliser `tmux` pour partager une session terminal en temps réel
* utilisez la commande `tmux` pour partager un terminal
