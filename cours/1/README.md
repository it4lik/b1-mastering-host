# Cours 1 : Architecture d'un ordinateur

Les slides du cours sont dispos :
* [en ligne](https://slides.com/hit4leak/computer-arch)
* [exportées en HTML](./slides/computer-arch.html)

# Notions abordées en cours

* architecture physique d'un ordinateur
  * définition "ordinateur"
  * exploration des principaux composants d'un ordinateur
  * mention de certaines notions de théorie de l'informatique
    * loi de Moore
    * machine de Turing
* fonctionnement d'un CPU
  * fonctionnement théorique
    * portes logiques
    * implémentation physique (utilisation de transistors)
  * rôle dans l'ordinateur
  * bus
  * discussion autour des vulnérabilités Spectre et Meltdown
  * architecture CPU
    * RISC/CISC
    * 32/64 bits
* fonctionnement du stockage
  * RAM
  * registres CPU
  * concept de cache
  * HDD
    * partitionnement
  * théorie du stockage
    * fonctionnement physique d'un disque dur
    * FILO/FIFO
    * heap, stack in RAM
* software
  * qu'est ce qu'un "programme"
  * différents types de programmes
    * drivers
    * firmwares
    * bootloaders
    * os
    * kernel
    * application
  * rôle de l'OS et du noyau
    * appels système
  * interaction avec le matériel
* assembleur
  * visualisation de l'assembleur
  * explication succincte de la compilation
  * example de désassemblage/débuggage d'un programme compilé
